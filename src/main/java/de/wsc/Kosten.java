package de.wsc;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Kosten {
    private long id;
    private LocalDate date;
    private String geschaeft;
    private Kategorie kategorie;
    private BigDecimal preis;
    private String bemerkung;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getGeschaeft() {
        return geschaeft;
    }

    public void setGeschaeft(String geschaeft) {
        this.geschaeft = geschaeft;
    }

    public Kategorie getKategorie() {
        return kategorie;
    }

    public void setKategorie(Kategorie kategorie) {
        this.kategorie = kategorie;
    }

    public BigDecimal getPreis() {
        return preis;
    }

    public void setPreis(BigDecimal preis) {
        this.preis = preis;
    }

    public String getBemerkung() {
        return bemerkung;
    }

    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }
}
