package de.wsc;


import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("v1")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KostenResource {

    @Inject
    private KostenStore kostenStore;

    public KostenResource() {
    }

    @GET
    @Path("/kosten")
    public List<Kosten> getAll() {
        return kostenStore.getKosten();
    }

    @POST
    @Path("/kosten")
    public void createKosten(Kosten kosten) {
        kostenStore.addKosten(kosten);
    }

    @DELETE
    @Path("/kosten")
    public void deleteKosten(long id) {
        kostenStore.deleteKosten(id);
    }
}
