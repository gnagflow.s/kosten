package de.wsc;

import javax.enterprise.context.ApplicationScoped;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class KostenStore {
    private final List<Kosten> kostenList = new ArrayList<>();

    public KostenStore() {
        Kosten kosten = new Kosten();
        kosten.setBemerkung("Bemerkung");
        kosten.setDate(LocalDate.now());
        kosten.setGeschaeft("Ebl");
        kosten.setId(0);
        kosten.setPreis(new BigDecimal("1.2"));
        kostenList.add(kosten);
        kosten = new Kosten();
        kosten.setBemerkung("Bemerkung 1");
        kosten.setDate(LocalDate.now());
        kosten.setGeschaeft("Ebl");
        kosten.setId(1);
        kosten.setPreis(new BigDecimal("9.99"));
        kostenList.add(kosten);
    }

    public List<Kosten> getKosten() {
        return new ArrayList<>(kostenList);
    }

    public void addKosten(Kosten kosten) {
        kosten.setId(getMaxId());
        kostenList.add(kosten);
    }

    public void deleteKosten(long id) {
        for (Kosten kosten : kostenList) {
            if (kosten.getId() == id) {
                kostenList.remove(kosten);
                break;
            }
        }
    }

    private long getMaxId() {
        long id = 0;
        for (Kosten kosten : kostenList) {
            long kostenId = kosten.getId();
            if (kostenId > id) {
                id = kostenId;
            }
        }
        return id+1;
    }
}
