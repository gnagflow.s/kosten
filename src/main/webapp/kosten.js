window.kostenApp = new Vue({
    el: "#kostenApp",
    data() {
        return {
            kosten: null
        }
    },
    mounted() {
        axios.get("http://localhost:9080/api/v1/kosten")
            .then(response => this.kosten = response.data)
    }
})